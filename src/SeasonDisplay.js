import React from "react";
import "./SeasonDisplay.css";
const seasonConfig = {
  summer: {
    text: "Summer !",
    iconName: "sun",
  },
  winter: {
    text: "Winter !",
    iconName: "snowflake",
  },
};

const getSeason = (lat, month) => {
  if (month > 2 && month < 9) {
    return lat > 0 ? "summer" : "winter";
  } else {
    return lat > 0 ? "winter" : "summer";
  }
};

const SeasonDisplay = (props) => {
  const season = getSeason(props.lat, new Date().getMonth());
  const { text, iconName } = seasonConfig[season];
  console.log(props);

  return (
    <div className={`season-display  ${season}`}>
      <i className={`massive season-display ${iconName} icon-left icon`} />
      <h1>
        {text}
        <br />
        Today :{props.today}
        <br />
        Latitude :{props.lat}
        <br />
        {props.time}
      </h1>

      <i className={` massive season-display ${iconName}  icon-right icon`} />
    </div>
  );
};

export default SeasonDisplay;
