import React from "react";
import ReactDOM from "react-dom";
import "semantic-ui-css/semantic.min.css";
import SeasonDisplay from "./SeasonDisplay";
import Spinner from "./Spinner";
class App extends React.Component {
  state = { lat: null, errorMessage: "", today: null, time: null };

  componentDidMount() {
    window.navigator.geolocation.getCurrentPosition(
      (position) =>
        this.setState({
          lat: position.coords.latitude,
          log: console.log("logitutde:" + position.coords.latitude),
        }),
      (err) => this.setState({ errorMessage: err.message })
    );
    setInterval(() => {
      this.setState(
          {time: new Date().toLocaleTimeString()} )   
  }, 1000)


    this.setState({ today: new Date().toString() });
    //this.setState({ time: new Date().toLocaleTimeString() });
  }
  renderContent() {
    if (this.state.errorMessage && !this.state.lat) {
      return <div>Error: {this.state.errorMessage}</div>;
    }

    if (!this.state.errorMessage && this.state.lat) {
      return <SeasonDisplay lat={this.state.lat} today={this.state.today} 
      time={this.state.time}/>;
    }

    return <Spinner message="Please Accept Location Request" />;
  }

  // React says we have to define render!!
  render() {
    return (
      <div style={{ border: "5px solid blue" }}>{this.renderContent()}</div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
